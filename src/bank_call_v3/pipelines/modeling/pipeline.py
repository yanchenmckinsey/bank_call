from kedro.pipeline import Pipeline, node
from .nodes import *

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                generate_tf_matrix,
                ["post_pos", "parameters"],
                ["term_freq_matrix", "term_freq_model"],
                name="generate_tf_matrix",
            ),
            node(
                LDA_modeling,
                ["term_freq_matrix", "term_freq_model", "parameters"],
                ["lda_output", "lda_plot"],
                name="LDA_modeling",
            ),
        ]
    )
