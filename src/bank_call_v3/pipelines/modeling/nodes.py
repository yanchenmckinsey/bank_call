from sklearn.feature_extraction.text import CountVectorizer # Word frequency

from sklearn.decomposition import LatentDirichletAllocation

import pyLDAvis.sklearn


def generate_tf_matrix(df, parameter):
    max_df = parameter['max_df']
    max_features = parameter['max_features']
    min_df = parameter['min_df']
    ngram_range = parameter['n_gram_range']
    text_col = parameter['preprocessed_column']
    text_data = df[text_col]

    # LDA requires integer values -> Use word frequency (CountVectorizer)
    tf = CountVectorizer(max_df=max_df,
                         # Ignore terms (words) that have a document frequency strictly higher than the given threshold
                         max_features=max_features,  # Set limit for words
                         min_df=min_df,
                         # Ignore terms (words) that have a document frequency strictly lower than the given threshold
                         ngram_range=ngram_range  # Include N-grams up to three
                         )

    tf_matrix = tf.fit_transform(text_data)  # Generate doc/word matrix

    return tf_matrix, tf


def LDA_modeling(tf_matrix, tf_model, parameter):

    num_cluster = parameter['num_cluster']

    lda = LatentDirichletAllocation(n_components=num_cluster)

    # Use LDA for clustering
    lda_output = lda.fit_transform(tf_matrix)

    # Use pyLDA to visulize clusters and their key words
    lda_plot = pyLDAvis.sklearn.prepare(lda, tf_matrix, tf_model, sort_topics=False)

    return lda_output, lda_plot