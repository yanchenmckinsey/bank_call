from kedro.pipeline import Pipeline, node
from .nodes import *

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                summarization,
                ["transcript_data", "parameters"],
                ["post_summarization", "N_gram_dict_summarization", 'plot_summarization'],
                name="summarization",
            ),
            node(
                normalization,
                ["post_summarization", "parameters"],
                ["post_normalization","N_gram_dict_normalization", 'plot_normalization'],
                name="normalization",
            ),
            node(
                stopword_removal,
                ["post_normalization", "parameters"],
                ["post_stopword","N_gram_dict_stopword", 'plot_stopword'],
                name="stopword_removal",
            ),
            node(
                lemmatization,
                ["post_stopword", "parameters"],
                ["post_lemmatization","N_gram_dict_lemmatization", 'plot_lemmatization'],
                name="lemmatization",
            ),
            node(
                pos_filtering,
                ["post_lemmatization", "parameters"],
                ["post_pos","N_gram_dict_pos", 'plot_pos'],
                name="pos_filtering",
            ),
        ]
    )
