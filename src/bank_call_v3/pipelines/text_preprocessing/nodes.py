# NLP - Text preprocessing
import re

import string
punctuation = set(string.punctuation)

from spellchecker import SpellChecker
from textblob import TextBlob
from collections import Counter
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))

from nltk.stem import SnowballStemmer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from sumy.summarizers.text_rank import TextRankSummarizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer

nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
nltk.download('punkt')

import spacy


def combine_docs(df_col):
    all_docs = ''.join([doc for doc in df_col])
    return all_docs


def extract_ngrams(data, num):
    n_grams = TextBlob(data).ngrams(num)
    return [' '.join(grams) for grams in n_grams]


def generate_top_N_table(gram_list):
    word_count_dict = dict(Counter(gram_list))
    word_count_df = pd.DataFrame.from_dict(word_count_dict, orient='index', columns=['word_count'])
    word_count_df = word_count_df.sort_values(by='word_count', ascending=False)
    return word_count_df


def generate_gram_dict(df_col, N):
    all_trans = combine_docs(df_col)

    grams_dict = {}
    n_grams = list(range(1, N + 1))

    for gram in n_grams:
        gram_name = str(gram) + '_gram'
        gram_list = extract_ngrams(all_trans, gram)
        grams_dict[gram_name] = generate_top_N_table(gram_list)
    return grams_dict

def text_summarize(text, para):
    summarizer = para['text_summarizer']
    num_sentence = para['num_sentence']
    parser = PlaintextParser.from_string(text, Tokenizer("english"))

    text_summary = ''
    summary = summarizer(parser.document, num_sentence)
    for sentence in summary:
        text_summary += str(sentence) + '\n'
    return text_summary


def normalize_text(line):
    """ lowers text and removes punctuation

    Parameters:
    line : string

    Returns: normalized string
    """
    return "".join(ch
                   for ch in line.lower()
                   if ch not in punctuation
                   )


def remove_stopwords(line, para):
    """ remove stopwords from a string

    Parameters:
    line : string

    Returns: string
    """
    stop_words = para['stop_words']
    extra_stop_words = para['extra_stop_words']
    stop_words.extend(extra_stop_words)

    return " ".join(
        [word
         for word in line.split()
         if word not in stop_words
         ])


def remove_URL(line):
    """ removes URL from a string

    Parameters:
    line : string

    Returns: string
    """
    return re.sub(r"http\S+", "", line)


def remove_continuous_characters(line):
    """Only alphanumeric words are matched with this pattern:
            \s* - zero or more whitespaces
            \b - word boundary
            (?=
            \b[a-z\d]*([a-z\d])\1{1}\b
            |
            \d+|[a-z]\b
            |
            \S*\d\S*
            )
            [a-z\d]+ - a word with 1+ letters or digits.

    """
    return re.sub(r"\s*\b(?=\b[a-z\d]*([a-z\d])\1{1}\b|\d+|[a-z]\b|\S*\d\S*)[a-z\d]+", "", line)


def correct_spellings(text):
    """ corrects spellings of the document
    Note: the string processing is slow in this function

    Parameters:
    text : string

    Returns: spell corrected string
    """

    spell = SpellChecker()
    corrected_text = []

    # create a list of misspelled words in the text
    misspelled_words = spell.unknown(text.split())

    # check if the word is misspelled, then correct it
    for word in text.split():
        if word in misspelled_words:
            corrected_text.append(spell.correction(word))
        else:
            corrected_text.append(word)

    return " ".join(corrected_text)


def wordnet_lemmatizer(line):
    """ reduces the inflected words properly ensuring that the root word belongs to the language

    Parameters:
    text : string

    Returns: processed string
    """
    # init WordNetlemmatizer
    lemmatizer = WordNetLemmatizer()
    # lemmatize list of words and join
    return ' '.join([
        lemmatizer.lemmatize(word)
        for word in line.split()
    ])


def snowball_stemmer(line, language="english"):
    """ reducing inflection in words to their root forms even if the stem itself is not a valid word in the Language

    Parameters:
    text : string

    Returns: processed string
    """
    stemmer = SnowballStemmer(language)

    return ' '.join([
        stemmer.stem(word)
        for word in line.split()
    ])


def pos_tagging(text, para):
    nlp = para['spacy_nlp']
    focus_pos = para['focus_pos']

    processed_text = []
    for token in nlp(text):
        if token.pos_ in focus_pos:
            processed_text.append(token.text)
    processed_string = ' '.join(processed_text)
    return processed_string

def create_summary_plot(n_gram_res):
    fig,axes=plt.subplots(figsize=(20,20),nrows=3,ncols=2)
    for i, time in enumerate(n_gram_res.keys()):
        for j, n in enumerate(n_gram_res[time].keys()):
            data = n_gram_res[time][n][:20]
            sns.barplot(y=data.index, x='word_count',data=data, color='blue',alpha=0.5, orient='h',ax=axes[j][i]).set(title=time + '-' + n)
    return fig

def summarization(df, parameters):
    is_summarize = parameters['is_summarize']
    preprocessed_col = parameters['preprocessed_column']
    is_EDA = parameters['is_summarization_EDA']
    N_gram = parameters['N_gram_EDA']
    text_col = parameters['text_log_column']
    parameters['text_summarizer'] = TextRankSummarizer()

    df[preprocessed_col] = df[text_col]
    grams_dict = {}

    if is_EDA:
        grams_dict['before_summarization'] = generate_gram_dict(df[preprocessed_col], N_gram)

    if is_summarize:
        df[preprocessed_col] = df.apply(lambda x: text_summarize(x[preprocessed_col], parameters), axis=1)

    if is_EDA:
        grams_dict['after_summarization'] = generate_gram_dict(df[preprocessed_col], N_gram)

    grams_plot = create_summary_plot(grams_dict)

    return df, grams_dict, grams_plot

def normalization(df, parameters):
    is_normalize = parameters['is_normalize']
    preprocessed_col = parameters['preprocessed_column']
    is_EDA = parameters['is_normalization_EDA']
    N_gram = parameters['N_gram_EDA']
    parameters['text_summarizer'] = TextRankSummarizer()

    grams_dict = {}

    if is_EDA:
        grams_dict['before_normalization'] = generate_gram_dict(df[preprocessed_col], N_gram)

    if is_normalize:
        df[preprocessed_col] = df[preprocessed_col].apply(normalize_text)

    if is_EDA:
        grams_dict['after_normalization'] = generate_gram_dict(df[preprocessed_col], N_gram)

    grams_plot = create_summary_plot(grams_dict)

    return df, grams_dict, grams_plot

def stopword_removal(df, parameters):
    is_stopword = parameters['is_stopword']
    preprocessed_col = parameters['preprocessed_column']
    is_EDA = parameters['is_stopword_EDA']
    N_gram = parameters['N_gram_EDA']
    parameters['stop_words'] = stopwords.words('english')

    grams_dict = {}

    if is_EDA:
        grams_dict['before_stopword'] = generate_gram_dict(df[preprocessed_col], N_gram)

    if is_stopword:
        df[preprocessed_col] = df.apply(lambda x: remove_stopwords(x[preprocessed_col], parameters), axis=1)

    if is_EDA:
        grams_dict['after_stopword'] = generate_gram_dict(df[preprocessed_col], N_gram)

    grams_plot = create_summary_plot(grams_dict)

    return df, grams_dict, grams_plot

def lemmatization(df, parameters):
    is_lemmatize = parameters['is_lemmatize']
    preprocessed_col = parameters['preprocessed_column']
    is_EDA = parameters['is_lemmatization_EDA']
    N_gram = parameters['N_gram_EDA']

    grams_dict = {}

    if is_EDA:
        grams_dict['before_lemmatization'] = generate_gram_dict(df[preprocessed_col], N_gram)

    if is_lemmatize:
        df[preprocessed_col] = df[preprocessed_col].apply(wordnet_lemmatizer)

    if is_EDA:
        grams_dict['after_lemmatization'] = generate_gram_dict(df[preprocessed_col], N_gram)

    grams_plot = create_summary_plot(grams_dict)

    return df, grams_dict, grams_plot

def pos_filtering(df, parameters):
    is_pos = parameters['is_pos']
    preprocessed_col = parameters['preprocessed_column']
    is_EDA = parameters['is_pos_EDA']
    N_gram = parameters['N_gram_EDA']
    parameters['spacy_nlp'] = spacy.load('en_core_web_lg')

    grams_dict = {}

    if is_EDA:
        grams_dict['before_pos'] = generate_gram_dict(df[preprocessed_col], N_gram)

    if is_pos:
        df[preprocessed_col] = df.apply(lambda x: pos_tagging(x[preprocessed_col], parameters), axis=1)

    if is_EDA:
        grams_dict['after_pos'] = generate_gram_dict(df[preprocessed_col], N_gram)

    grams_plot = create_summary_plot(grams_dict)

    return df, grams_dict, grams_plot