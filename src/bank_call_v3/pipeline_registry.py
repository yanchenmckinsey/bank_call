"""Project pipelines."""
from typing import Dict

from kedro.pipeline import Pipeline

from bank_call_v3.pipelines import text_preprocessing
from bank_call_v3.pipelines import modeling


def register_pipelines() -> Dict[str, Pipeline]:
    """Register the project's pipelines.

    Returns:
        A mapping from a pipeline name to a ``Pipeline`` object.
    """
    text_preprocessing_pipeline = text_preprocessing.create_pipeline()
    topic_modeling_pipeline = modeling.create_pipeline()


    return {
        "text_preprocessing": text_preprocessing_pipeline,
        "topic_modeling": topic_modeling_pipeline,
        "__default__": text_preprocessing_pipeline + topic_modeling_pipeline,
    }
